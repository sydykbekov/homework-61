import React from 'react';
import './CountryList.css';

const CountryList = (props) => (
    props.countries.map((item, i) => <li onClick={() => props.handleClick(i)} key={i}>{item.name}</li>)
);

export default CountryList;