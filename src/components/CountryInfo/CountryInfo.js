import React from 'react';
import './CountryInfo.css';

const CountryInfo = props => {
    if (props.country) {
        return (
            <div className="countryInfo">
                <img src={props.country.info.flag} alt="flag"/>
                <h3>Country: {props.country.info.name}</h3>
                <h4>Capital: {props.country.info.capital}</h4>
                <h4>Borders: </h4>
                {props.country.borders.map((value, k) => <p key={k}>{value}</p>)}
            </div>
        )
    }
    return <div className="countryInfo"><b>Please choose country!</b></div>;
};

export default CountryInfo;