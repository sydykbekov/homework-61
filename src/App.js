import React, {Component} from 'react';
import CountryList from './components/CountryList/CountryList';
import CountryInfo from './components/CountryInfo/CountryInfo';
import './App.css';

class App extends Component {
    state = {
        countries: [],
        countryInfo: null
    };

    componentDidMount() {
        const URL = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
        fetch(URL).then(response => {
            return response.json();
        }).then(countries => {
            this.setState({countries});
        });
    }

    getCountryInfo = (index) => {
        let countryInfo = {
            info: '',
            borders: []
        };
        let promises = [];
        fetch('https://restcountries.eu/rest/v2/alpha/' + this.state.countries[index].alpha3Code).then(response => {
            return response.json();
        }).then(country => {
            countryInfo.info = country;
            for (let i = 0; i < country.borders.length; i++) {
                promises.push(fetch('https://restcountries.eu/rest/v2/alpha/' + country.borders[i]).then(response => {
                    return response.json();
                }));
            }
            return Promise.all(promises);
        }).then(borders => {
            for (let i = 0; i < borders.length; i++) {
                countryInfo.borders.push(borders[i].name);
            }
            this.setState({countryInfo});
        });
    };

    render() {
        return (
            <div className="App">
                <ul className="countryList">
                    <CountryList handleClick={this.getCountryInfo} countries={this.state.countries} />
                </ul>
                <CountryInfo country={this.state.countryInfo} />
            </div>
        );
    }
}

export default App;
